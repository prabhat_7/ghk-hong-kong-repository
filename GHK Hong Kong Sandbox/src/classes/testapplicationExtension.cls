@isTest
private class testapplicationExtension
{
	@isTest
	static void itShould()
	{


                     Applications__c app=new Applications__c();

                     app.Risk__c='HGI';
                     app.HK_Specialist_Registration_in__c='S06 Obstetrics & Gynaecology';
                     app.Fellowship_of_HK_Specialist__c='S05 Family Medicine (FHKCFP)';
                     app.Other_Specialist_Qualifications__c='test';
                     app.MPS_No__c='HK12443';
                     app.Other_No__c='WER54674';
                     app.MPS_Expiry_Date__c=System.Today();
                     app.Name__c='test name';
                     app.MCHK_Code__c='tt';
                     //app.Specialist_Registration_No__c=
                     app.Date_of_Birth__c=System.Today();
                     app.Gender__c='Male';
                     app.Marital_Status__c='single';
                     app.University_Attended__c='None';
                     app.Year_of_Graduation__c=1990;
                     app.Degree_Obtained__c='None';
                     app.Date_of_Registration_HKU__c=System.Today();
                     app.Registration_No_HKU__c='123123';
                     app.Qualification_Used__c='none';

                     insert app;


                     Admission_Rights__c admr=new Admission_Rights__c();
                     admr.Application__c=app.id;
                     admr.Current__c=True;
                     admr.Hospitals_in_Hong_Kong__c='Non-local Hospitals';
                     admr.Reason_for_Cessation__c='none';
                     admr.Non_Local_Hospital__c='none';

                     insert admr;


                     Privileges_Connections__c pc=new Privileges_Connections__c();
                     pc.Application__c=app.id;
                     pc.Core_Special__c='Special';
                     pc.Initial_Criteria__c='test initial criteria';
                     pc.Procedures__c='test procedures';

                     insert pc;


                     Teaching_Experience__c teaching=new Teaching_Experience__c();
                     teaching.Name='test';
                     teaching.Application__c=app.id;
                     teaching.Date_From__c=System.Today();
                     teaching.Date_To__c=System.Today();
                     teaching.Educational_Activities__c='test acitivites';
                     teaching.Participation__c='test participation';
                     teaching.Professional_Body__c='test professional body';

                     insert teaching;


                     Professional_Services__c ps=new Professional_Services__c();
                     ps.Name='test professional name';
                     ps.Application__c=app.id;
                     ps.Dates_and_Place__c='test date and place';
                     ps.Name_Type_of_Service__c='test name type of services';
                     ps.Role_of_Involvement__c='test role of involvement';

                     insert ps;

                     Work_Experience__c work=new Work_Experience__c();
                     work.Application__c=app.id;
                     work.Date_From__c=System.today();
                     work.Date_To__c=System.today();
                     work.Employment_Institution__c='test employement instute';
                     work.Part_Time__c=true;
                     work.Position_Held_and_Specialty__c='test position held';

                     insert work;


                     Referees__c ref=new Referees__c();

                     ref.Name='test ref name';
                     ref.Position__c='test position';
                     ref.Correspondence_Address__c='test correspondenceAddress';
                     ref.Email_Address__c='test@gmail.com';
                     ref.Telephone_Number__c='987654';
                     ref.Fax__c='98765';
                     ref.Relationship_with_Applicant__c='none';
                     ref.Applications__c=app.id;

                     insert ref;


                     Qualification__c qual=new Qualification__c();
                     qual.Name='test qualification';
                     qual.Application__c=app.id;
                     qual.Completion_Date__c=System.today();


                     insert qual;



                     Address__c addr=new Address__c();
                     addr.City__c='test city';
                     addr.Fax_Number__c='987654567';
                     addr.Contact_No__c='987656789';
                     addr.Country__c='jhgfghjk';
                     addr.State_Provinance__c='kjhgfghjk';
                     addr.Street__c='kjhgf';
                     addr.Type__c='okjhg';
                     addr.Zip_Postal_Code__c='09876';
                     addr.Application__c=app.id;


                     insert addr;


                     ApexPages.StandardController appCntrl = new ApexPages.StandardController(app);
                     applicationExtension appExtension=new applicationExtension(appCntrl);




	}
}