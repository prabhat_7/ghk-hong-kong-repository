/*
//Author Sudhir Dudeja
//Description It is a trigger handler which will handle the trigger calls.
 */

public with sharing class triggerHandler {

           //Need to create a map of application and privilges values

	public triggerHandler() {

	}
            //This function is for the application trigger which would used to create privileges connections
            //public void createPrivilegesConnections(List<Applications__c> newRecordsList, List<Applications__c> oldRecordsList, Map<Id, Applications__c> newMap, Map<Id, Applications__c> oldMap, String eventType, String triggerType){

            //         Set<ID> specialityID=new Set<ID >();
            //         Map<ID,List<Privileges__c>> privilegesIDMap=new Map<ID,List<Privileges__c>>();

            //         List<Privileges_Connections__c> privilegesConnectionList=new List<Privileges_Connections__c>();

            //         for(Applications__c app : newRecordsList){

            //                    if(app.Specialty__c!=null){

            //                              specialityID.add(app.Specialty__c);

            //                    }

            //         }

            //         if(!specialityID.isEmpty()){

            //                    System.debug('specialityID are'+specialityID);

            //                    for(Specialty__c special :  [SELECT Id, (SELECT Id,Specialty__c FROM Privileges__r) FROM Specialty__c]){

            //                              privilegesIDMap.put(special.id, special.Privileges__r);

            //                    }
            //         }


            //         if(!privilegesIDMap.values().isEmpty()){

            //                    System.debug('privilegesIDMap are'+ privilegesIDMap);

            //                    for(Applications__c app  :  newRecordsList){



            //                              for(Privileges__c privRecord  :  privilegesIDMap.get(app.Specialty__c)){

            //                                         Privileges_Connections__c privConnection=new Privileges_Connections__c();
            //                                         privConnection.Application__c=app.id;
            //                                         privConnection.Priviliges__c=privRecord.id;
            //                                         privilegesConnectionList.add(privConnection);

            //                              }
            //                    }
            //         }

            //         if(!privilegesConnectionList.isEmpty()){

            //                    System.debug('privilegesConnectionList are'+privilegesConnectionList);

            //                    insert privilegesConnectionList;
            //         }

            //}
            //

            public void fetchPrivilegesValues(List<Privileges_Connections__c> newRecordsList, List<Privileges_Connections__c> oldRecordsList, Map<Id, Privileges_Connections__c> newMap, Map<Id, Privileges_Connections__c> oldMap, String eventType, String triggerType){

                     Map<ID,Privileges__c> privilgesMap=new Map<ID,Privileges__c>();


                                for(Privileges_Connections__c priv :  [Select id,name,Priviliges__c,Priviliges__r.Core_Special__c,Priviliges__r.Initial_Criteria__c,Priviliges__r.Procedures__c from Privileges_Connections__c where id=:newRecordsList]){

                                            //System.debug('privileges list is'+ priv.Priviliges__r.Core_Special__c);
                                            privilgesMap.put(priv.Priviliges__c, priv.Priviliges__r);

                                            System.debug('privileges map is'+privilgesMap);

                                }

                    if(!privilgesMap.keySet().isEmpty()){

                                for(Privileges_Connections__c priv : newRecordsList){

                                         if(privilgesMap.containsKey(priv.Priviliges__c)){

                                                     priv.Core_Special__c=privilgesMap.get(priv.Priviliges__c).Core_Special__c;
                                                     priv.Initial_Criteria__c=privilgesMap.get(priv.Priviliges__c).Initial_Criteria__c;
                                                     priv.Procedures__c=privilgesMap.get(priv.Priviliges__c).Procedures__c;

                                                     //System.debug('core special is'+priv.Priviliges__r.Core_Special__c);
                                                     //System.debug('core special is'+priv.Priviliges__r);
                                                     //System.debug('priv is'+priv);

                                         }
                                }

                    }



            }

            //This function handle the call of applicationSepciality trigger
            public void createApplicationsPrivilegesConnections(List<Application_Speciality__c> newRecordsList, List<Application_Speciality__c> oldRecordsList, Map<Id, Application_Speciality__c> newMap, Map<Id, Application_Speciality__c> oldMap, String eventType, String triggerType){

                     Set<ID> appID=new Set<ID>();
                     Set<ID> specialityID=new Set<ID>();
                     Map<ID,List<Privileges__c>> privilegesIDMap=new Map<ID,List<Privileges__c>>();
                     List<Privileges_Connections__c> privilegesConnectionList=new List<Privileges_Connections__c>();


                     if(appID.isEmpty() ||  specialityID.isEmpty()){


                                for(Application_Speciality__c app : newRecordsList){

                                          if(app.Specialty__c!=null){

                                                     specialityID.add(app.Specialty__c);

                                          }
                                          if(app.Application__c!=null){

                                                    appID.add(app.Application__c);
                                          }

                                 }

                     }


                     if(!specialityID.isEmpty()){

                                System.debug('specialityID are'+specialityID);

                                for(Specialty__c special :  [SELECT Id, (SELECT Id,Specialty__c FROM Privileges__r) FROM Specialty__c]){

                                          privilegesIDMap.put(special.id, special.Privileges__r);

                                }
                      }


                      if(!privilegesIDMap.values().isEmpty()){

                               System.debug('privilegesIDMap are'+ privilegesIDMap);

                                for(Application_Speciality__c app  :  newRecordsList){



                                          for(Privileges__c privRecord  :  privilegesIDMap.get(app.Specialty__c)){

                                                     Privileges_Connections__c privConnection=new Privileges_Connections__c();
                                                     privConnection.Application__c=app.Application__c;
                                                     privConnection.Priviliges__c=privRecord.id;
                                                     privilegesConnectionList.add(privConnection);

                                           }
                                }
                      }



                      if(!privilegesConnectionList.isEmpty()){

                                System.debug('privilegesConnectionList are'+privilegesConnectionList);

                                insert privilegesConnectionList;

                      }



            }

}