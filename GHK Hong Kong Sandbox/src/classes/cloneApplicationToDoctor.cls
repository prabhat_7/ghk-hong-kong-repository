public class cloneApplicationToDoctor {

    public cloneApplicationToDoctor(ApexPages.StandardController controller) {

    }


           public ID applicationID {get; set;}

           public string message {get; set;}


           public ID accountID {get; set;}

           public cloneApplicationToDoctor() {

                     message='test';

            }


            public void doctorInformation(){

                           applicationID = ApexPages.currentPage().getParameters().get('Id');

                           if(applicationID==null){
                           message='Cannot find Valid ID';
                           }


                           String theQuery='Select ';

                           System.debug('applicationID is'+applicationID);

                           Set<String> appFieldSet = fetchFieldsApiNames('Applications__c');

                           System.debug('appFieldSet is'+appFieldSet);

                         if(!appFieldSet.isEmpty()){

                                   for(String fieldName1 : appFieldSet) {
                                           theQuery += fieldName1 + ',';
                                    }

                         }


                         theQuery = theQuery.subString(0, theQuery.length()-1);

                         theQuery += ' from Applications__c where ID=: applicationID';

                        System.debug('the query is'+theQuery);

                        //Casting the specific list<sObject> into application list
                        List<Applications__c> appList = (List<Applications__c>)Database.query(theQuery);


                        Set<String> accountFieldSet = fetchFieldsApiNames('Account');


                        Account acc=new Account();

                            if(appList.size() > 0){

                                   Applications__c app =  appList.get(0);
                                   for(String appF : appFieldSet){

                                               if(accountFieldSet.contains(appF)){

                                                               acc.put(appF,app.get(appF));
                                               }

                                   }

                                   System.debug('acc is'+acc);

                                   if(acc!=null){

                                            try {

                                                        Database.SaveResult saveResult = Database.insert(acc, false);



                                                        if(saveResult.isSuccess()){

                                                                  System.debug('Successfully inserted account Account ID: ' + saveResult.getId());

                                                        }

                                                        else {

                                                                  for(Database.Error err : saveResult.getErrors()){

                                                                            System.debug('The following error has occurred.');

                                                                            System.debug(err.getStatusCode() + ': ' + err.getMessage());

                                                                            System.debug('Account fields that affected this error: ' + err.getFields());

                                                                            message='There is an Error '+  err.getMessage();

                                                                  }
                                                        }



                                                        childObjects(applicationID, saveResult.getId());

                                                        message='Doctor Record get created successfully, ID: '+ saveResult.getId();

                                                        accountID=SaveResult.getId();

                                            } catch(Exception e) {

                                                       System.debug(e.getMessage());

                                                       message='There is an Error '+ e.getMessage();
                                            }

                                   }
                         }


            }


            public static set<String> fetchFieldsApiNames(String SobjectApiName) {

                      Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
                      Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();


                      Set<String> fieldsName=new Set<String>();


                      System.debug('fieldmap value are'+fieldmap.Values());


                      System.debug('fieldmap keys are'+fieldmap.Keyset());


                     if (fieldMap != null){
                                 for (Schema.SObjectField ft : fieldMap.values()){ // loop through all field tokens (ft)
                                           Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                                           if (fd.isCreateable()){ // field is creatable
                                                      fieldsName.add(fd.getName());
                                           }
                                 }
                       }


                       if(!fieldsName.isEmpty()){

                                 if(fieldsName.contains('RecordTypeId')){

                                           fieldsName.remove('RecordTypeId');
                                 }

                       }

                      System.debug('fieldsName are'+fieldsName);

                      return fieldsName;

             }


                public void childObjects(ID applicationID,ID accountID){


                    List<Qualification__c> qualList=new List<Qualification__c>();

                    List<Professional_Services__c> professionalServicesList=new List<Professional_Services__c>();

                    List<Admission_Rights__c> admissionRightsList=new List<Admission_Rights__c>();

                    List<Application_Speciality__c> appSpecialitiesList=new List<Application_Speciality__c>();


                    List<Privileges_Connections__c> privConnectionList=new List<Privileges_Connections__c>();

                    List<Teaching_Experience__c> teachingList=new List<Teaching_Experience__c>();

                    List<Work_Experience__c> workList=new List<Work_Experience__c>();

                    List<Address__c> addressList=new List<Address__c>();

                    List<attachment> attachList=new List<Attachment>();

                     for(Applications__c app : [SELECT Name, (SELECT Account__c FROM Qualifications__r), (SELECT Doctor__c FROM Professional_Services__r), (SELECT Doctor__c FROM Admission_Rights__r), (SELECT Doctor__c FROM Application_Specialities__r), (SELECT Doctor__c FROM Work_Experience__r), (SELECT Doctor__c FROM Privileges_Connections1__r), (SELECT Doctor__c FROM Teaching_Experiences__r), (SELECT Doctor__c FROM Addresses__r) FROM Applications__c where id=:applicationID]) {

                                System.debug('pp.Privileges_Connections1__r'+app.Privileges_Connections1__r);
                                System.debug('pp.Qualifications__r'+app.Qualifications__r);
                                if(!app.Qualifications__r.isEmpty()){

                                            qualList.addAll(app.Qualifications__r);

                                }



                                if(!app.Professional_Services__r.isEmpty()){

                                            professionalServicesList.addAll(app.Professional_Services__r);
                                }

                                if(!app.Admission_Rights__r.isEmpty()){

                                        admissionRightsList.addAll(app.Admission_Rights__r);

                                }

                                if(!app.Application_Specialities__r.isEmpty()){


                                        appSpecialitiesList.addAll(app.Application_Specialities__r);

                                }

                                if(!app.Privileges_Connections1__r.isEmpty()){

                                          privConnectionList.addAll(app.Privileges_Connections1__r);

                                }
                                if(!app.Teaching_Experiences__r.isEmpty()){


                                          teachingList.addAll(app.Teaching_Experiences__r);

                                }

                                if(!app.Work_Experience__r.isEmpty()){

                                          workList.addAll(app.Work_Experience__r);

                                }

                                 if(!app.Addresses__r.isEmpty()){

                                          addressList.addAll(app.Addresses__r);

                                }

                     }

                      //if(attachList.isEmpty()){

                            for( Attachment att : [SELECT  id, name, body FROM Attachment where ParentId=:applicationID]){

                                      Attachment attach=new attachment();
                                      attach.Body=att.Body;
                                      attach.Name=att.Name;
                                      attach.ParentId=accountID;
                                      attachList.add(attach);
                            }
                      //}


                     if(!attachList.isEmpty()) {

                          System.debug('attachList is'+attachList);
                           insert attachList;

                     }


                     if(!addressList.isEmpty()){

                                for(Address__c addr : addressList){

                                         addr.Doctor__c=accountID;

                                }

                                update addressList;
                     }

                     if(!qualList.isEmpty()){

                                for(Qualification__c qual : qualList){

                                         qual.Account__c=accountID;

                                }

                                update qualList;
                     }

                     if(!professionalServicesList.isEmpty()){

                                for(Professional_Services__c pro : professionalServicesList){

                                         pro.Doctor__c=accountID;

                                }

                                update professionalServicesList;

                     }

                     if(!admissionRightsList.isEmpty()){

                                for(Admission_Rights__c adm : admissionRightsList){

                                         adm.Doctor__c=accountID;

                                }

                                update admissionRightsList;


                     }

                     if(!appSpecialitiesList.isEmpty()){

                                for(Application_Speciality__c app : appSpecialitiesList){

                                         app.Doctor__c=accountID;

                                }

                                update appSpecialitiesList;

                     }

                     if(!privConnectionList.isEmpty()){

                                for(Privileges_Connections__c priv : privConnectionList){

                                         priv.Doctor__c=accountID;

                                }

                                update privConnectionList;

                     }

                     if(!teachingList.isEmpty()){


                                for(Teaching_Experience__c teach : teachingList){

                                         teach.Doctor__c=accountID;

                                }

                                update teachingList;
                     }

                     if(!workList.isEmpty()){

                                for(Work_Experience__c work : workList){

                                         work.Doctor__c=accountID;

                                }

                                update workList;

                     }

            }

}