public class ApplicationCTRL {

    
    public Applications__c app{get;set;}
    public String det1{get;set;}
    public String appId{get;set;}
    public String det2{get;set;}
    public String det3{get;set;}
    public String det4{get;set;}
    public String det5 {get;set;}
   // public String hospital {get;set;}
    
    //list<Case> caseLst ;
    public ApplicationCTRL(){
     app=New Applications__c();  
      
            det1=apexpages.currentpage().getparameters().get('ApplicationsName');
            det2=apexpages.currentpage().getparameters().get('Name in Chinese');
            det3=apexpages.currentpage().getparameters().get('HKID');
            det4=apexpages.currentpage().getparameters().get('Passport Number');
            det5=apexpages.currentpage().getparameters().get('Country of Issue');
        
        
        system.debug('---det1--'+det1);
        if(det1!=null){
            app.Name=det1;
        }
        if(det2!=null){
            app.Name_in_Chinese__c=det2;
        }
        if(det3!=null){
            app.HKID__c=det3;
        }
        if(det4!=null){
            app.Passport_Number__c=det4;
        }
        if(det5!=null){
            app.Country_of_Issue__c = det5;
        }
        
        if(appId!=null){
            app = [Select Id,Name,Name_in_Chinese__c, HKID__c,Passport_Number__c,Country_of_Issue__c  From Applications__c Where Id =: appId];
            system.debug('app=== '+app);
            
        }
    }
}