/**********************************************************************************************
Author -:Sadre Alam
Created Date-:16/01/2017
Purpose-:This class is being used in File_Upload_Page to upload attachments from Application.
***********************************************************************************************/
public with sharing class AttachmentUploadController {

    public Id parId {get;set;}
    public attachment atch {get;set;}
    public boolean closewindow {get;set;}

    public AttachmentUploadController() {
        atch = New Attachment();
        closewindow = false;
        parId = apexpages.currentpage().getparameters().get('id');
        
        
    }
    public pageReference Save() {
        try {
            if (parId != null) {
                atch.Name+=atch.Name+'.jpg';
                atch.ParentId = parId;
                insert atch;
                list<Applications__c> applist = new list<Applications__c>([SELECT Id,Image_Id__c FROM Applications__c WHERE Id=:parId]);
                applist[0].Image_Id__c = atch.Id;
                update applist;
                atch.body = null;
                closewindow = true;
            }
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading attachment'));

        }

        return null;
    }

}