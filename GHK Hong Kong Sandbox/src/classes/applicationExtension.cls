public with sharing class applicationExtension {


    private final sObject mysObject;

    public List<Applications__c> appList {get;set;}

    public List<Address__c> addressList {get; set;}

    public string logourl{get;set;}
    public string point1 {get; set;}
    public string point2 {get; set;}
    public string point3 {get; set;}
    public string point4 {get; set;}
    public string point5 {get; set;}
    public string point6 {get; set;}
    public string point7 {get; set;}
    public string point8 {get; set;}
    public string point9 {get; set;}

    public Integer dayOfBirth {get;set;}

    public Integer monthOfBirth {get;set;}
    public string sign{get;set;}

    public Integer yearOfBirth {get;set;}

    //public String gender {get;set;}


    public Integer yearOfReg {get;set;}


    public String businessAddress {get;set;}

    public String residentAddress { get; set;}


    public String correspondenceAddress { get; set;}

    public List<Attachment> attachList {get; set;}

    public List<Qualification__c> qualList {get; set;}

    public List<Referees__c> refList {get;set;}


    public List<Work_Experience__c> workExpList { get; set;}


    public List<Professional_Services__c> professionalList {get;set;}


    public List<Teaching_Experience__c> teachingList {get;set;}

    public List<Admission_Rights__c> admissionList {get;set;}


    public List<Privileges_Connections__c> privilegesCoreList {get; set;}

    public List<Privileges_Connections__c> privilegesSpecialList {get; set;}

    public List<Admission_Rights__c> admissionListNonLocal {get;set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public applicationExtension(ApexPages.StandardController stdController) {



         this.mysObject = (sObject)stdController.getRecord();

         String pdfName= 'attachment; filename=Application - ' +(String)mysObject.get('Name')+'.pdf';
         Apexpages.currentPage().getHeaders().put('content-disposition',pdfName);



         appList=new List<Applications__c>();
         attachList=new List<Attachment>();
         qualList=new List<Qualification__c>();
         refList=new List<Referees__c>();
         workExpList=new List<Work_Experience__c>();
         professionalList=new List<Professional_Services__c>();
         teachingList=new List<Teaching_Experience__c>();
         admissionList=new List<Admission_Rights__c>();
         privilegesCoreList=new List<Privileges_Connections__c>();
         admissionListNonLocal=new List<Admission_Rights__c>();
         privilegesSpecialList=new List<Privileges_Connections__c>();

         logourl = label.logo_URL;
         logourl = label.logo_URL;
         sign= label.sign;
         point1=label.ApplicationPage_1_Point_1;
         point2=label.Application_Page_1_Point_2;
         point3=label.Application_Page_1_Point_3;
         point4=label.Application_Page1_Point_4;
         point5=label.Application_Page_1_Point_5;
         point6=label.Application_Page1_Point_6;
         point7=label.Application_Page1_Point_7;
         point8=label.Application_Page1_Point_8;
         point9=label.Application_Page1_Point_9;

         System.debug('id is'+(Id)mysObject.get('Id'));

         imageInfo();

         applicationList();

         addressInfo();

         qualificationInfo();

         refereesInfo();

         workExperience();

         professionalServices();

         teachingExperience();

         admissionInfo();

         privilegesCoreInfo();

         privilegesSpecialInfo();


         admissionInfoNonLocal();

    }

    public void admissionInfoNonLocal(){

            admissionListNonLocal=[Select id,Application__c,Current__c,Hospitals_in_Hong_Kong__c,Past__c,Reason_for_Cessation__c,Non_Local_Hospital__c from Admission_Rights__c where Application__c=:(Id)mysObject.get('Id') and Hospitals_in_Hong_Kong__c='Non-local Hospitals'];

    }

    public void privilegesSpecialInfo(){

           //privilegesSpecialList=[SELECT id,Application__c, Priviliges__r.Name,Priviliges__r.Core_Special__c,Priviliges__r.Initial_Criteria__c,Priviliges__r.Procedures__c,Priviliges__r.Remarks__c,Priviliges__r.Specialty__c FROM Privileges_Connections__c where Application__c=:(Id)mysObject.get('Id') And Priviliges__r.Core_Special__c='Special'];
           //
           privilegesSpecialList=[SELECT id,Application__c,Core_Special__c,Initial_Criteria__c,Procedures__c FROM Privileges_Connections__c where Application__c=:(Id)mysObject.get('Id') And Core_Special__c='Special' ORDER BY Order__c ASC ];

    }

    public void privilegesCoreInfo(){

            //privilegesCoreList=[SELECT id,Application__c, Priviliges__r.Name,Priviliges__r.Core_Special__c,Priviliges__r.Initial_Criteria__c,Priviliges__r.Procedures__c,Priviliges__r.Remarks__c,Priviliges__r.Specialty__c FROM Privileges_Connections__c where Application__c=:(Id)mysObject.get('Id') And Priviliges__r.Core_Special__c='Core'];
            //
              privilegesCoreList=[SELECT id,Application__c,Core_Special__c,Initial_Criteria__c,Procedures__c FROM Privileges_Connections__c where Application__c=:(Id)mysObject.get('Id') And Core_Special__c='Core' ORDER BY Order__c ASC ];
    }


    public void admissionInfo(){

        admissionList=[Select id,Application__c,Current__c,Hospitals_in_Hong_Kong__c,Past__c,Reason_for_Cessation__c from Admission_Rights__c where Application__c=:(Id)mysObject.get('Id') and Hospitals_in_Hong_Kong__c!='Non-local Hospitals'];

    }


    public void teachingExperience(){

         teachingList=[Select Id, Name,Application__c,Date_From__c,Date_To__c,Educational_Activities__c,Participation__c,Professional_Body__c from Teaching_Experience__c where Application__c=:(Id)mysObject.get('Id')];

    }



    public void professionalServices(){

            professionalList=[Select id,Name,Application__c,Dates_and_Place__c,Name_Type_of_Service__c,Role_of_Involvement__c from Professional_Services__c where Application__c=:(Id)mysObject.get('Id')];

    }



    public void workExperience(){

            workExpList=[Select Application__c,Date_From__c,Date_To__c,Employment_Institution__c,Part_Time__c,Position_Held_and_Specialty__c from Work_Experience__c where Application__c=:(Id)mysObject.get('Id')];

    }


    public void refereesInfo(){

        refList=[Select id,Name,Position__c,Correspondence_Address__c,Email_Address__c,Telephone_Number__c,Fax__c,Relationship_with_Applicant__c from Referees__c where Applications__c=:(Id)mysObject.get('Id')];

    }


    public void qualificationInfo(){

         qualList=[Select id,Name,Account__c,Application__c,Completion_Date__c from Qualification__c where Application__c=:(Id)mysObject.get('Id')];

    }

    public void imageInfo(){

            attachList=[SELECT Id, ContentType, ParentId FROM Attachment where ParentId=:(Id)mysObject.get('Id') limit 1];

    }


    public void applicationList(){

            appList=[Select id,Risk__c,HK_Specialist_Registration_in__c,Fellowship_of_HK_Specialist__c,Other_Specialist_Qualifications__c,MPS_No__c,Other_No__c,MPS_Expiry_Date__c, Name__c,MCHK_Code__c, Specialist_Registration_No__c,Date_of_Birth__c,Gender__c,Marital_Status__c,University_Attended__c,Year_of_Graduation__c ,Degree_Obtained__c,Date_of_Registration_HKU__c,Registration_No_HKU__c,Qualification_Used__c from Applications__c where id=:(Id)mysObject.get('Id') limit 1];

            System.debug('Risk is'+appList);


            if(!appList.isEmpty()){


                     for(Applications__c app : appList){

                               if(app.Date_of_Birth__c!=null){

                                            dayOfBirth=app.Date_of_Birth__c.day();
                                            monthOfBirth= app.Date_of_Birth__c.Month();
                                            yearOfBirth=app.Date_of_Birth__c.Year();

                                }

                                if(app.Date_of_Registration_HKU__c!=null){

                                          yearOfReg=app.Date_of_Registration_HKU__c.year();

                               }

                     }

                     System.debug('Risk is'+appList);

            }

    }


    public void addressInfo(){

          addressList=[Select id,City__c,Fax_Number__c,Contact_No__c,Country__c,State_Provinance__c,Street__c,Type__c,Zip_Postal_Code__c from Address__c where Application__c=:(Id)mysObject.get('Id') ];

          if(!addressList.isEmpty()){

            for(Address__c add :  addressList){

                     if(add.Type__c=='Business Address'){

                                businessAddress=add.Street__c + ' '+ add.City__c+ '  '+ add.State_Provinance__c + '  '+ add.Country__c + '  '+ add.Zip_Postal_Code__c;

                     }
                     else if(add.Type__c=='Resident Address'){

                                residentAddress=add.Street__c + ' '+ add.City__c+ '  '+ add.State_Provinance__c + '  '+ add.Country__c + '  '+ add.Zip_Postal_Code__c;
                     }
                     else if(add.Type__c=='Correspondence Address') {

                                correspondenceAddress=add.Street__c + ' '+ add.City__c+ '  '+ add.State_Provinance__c + '  '+ add.Country__c + '  '+ add.Zip_Postal_Code__c;
                     }

            }
          }

    }
}