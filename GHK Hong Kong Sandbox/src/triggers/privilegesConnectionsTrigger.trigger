trigger privilegesConnectionsTrigger on Privileges_Connections__c (before insert,before update) {


           TriggerHandler triggerHandler=new TriggerHandler();

           if(trigger.isBefore && trigger.isInsert) {

                      triggerHandler.fetchPrivilegesValues(trigger.new, null, trigger.newMap, null, 'Before', 'Insert');

            }

            if(trigger.isBefore && trigger.isUpdate) {

                      triggerHandler.fetchPrivilegesValues(trigger.new, null, trigger.newMap, null, 'Before', 'Update');

            }

}