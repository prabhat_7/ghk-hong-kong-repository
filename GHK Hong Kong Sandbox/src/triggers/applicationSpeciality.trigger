/*
@Developer Sudhir Dudeja
@Date 25th january 2017
 */

trigger applicationSpeciality on Application_Speciality__c (before insert,before update,after insert, after update) {

           TriggerHandler triggerHandler=new TriggerHandler();

           if(trigger.isAfter && trigger.isInsert) {

                     triggerHandler.createApplicationsPrivilegesConnections(trigger.new, null, trigger.newMap, null, 'After', 'Insert');

           }
}